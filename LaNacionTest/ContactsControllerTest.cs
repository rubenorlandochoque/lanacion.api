using LaNacion.Controllers;
using LaNacion.Models;
using LaNacion.Models.Contacts;
using Microsoft.EntityFrameworkCore;

namespace LaNacionTest
{
    public class ContactsControllerTest
    {

        private Repository<Contact> _repository;
        private ContactRepository _contactRepository;
        ContactsController _controller;
        public ContactsControllerTest()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName: "ApplicationDbContext")
            .Options;
            var context = new ApplicationDbContext(options);
            _repository = new Repository<Contact>(context);
            _contactRepository = new ContactRepository(context);
            _controller = new ContactsController(_repository, _contactRepository);
        }

        public async Task<int> insert(string id)
        {
            var result =  await  _controller.Insert(new Contact
            {
                Id = id,
                Name = "Ruben Orlando Choque",
                Address = "Salta",
                Company = "La Naci�n",
                Birthdate = new DateTime(1990, 2, 10),
                Email = "ruben@gmail.com",
                PhoneNumber = "3876896654",
                ProfileImage = "https://images/profile.jpg",
            });
            return result.Value;
        }
    

        [Fact]
        public async void TestInsert()
        {
            var id = Guid.NewGuid().ToString();
            var result = await insert(id);
            Assert.Equal(1, result);         
        }


        [Fact]
        public async void TestGetById()
        {
            var id = Guid.NewGuid().ToString();
            await insert(id);
            var result = await _controller.GetById(id);
            Assert.IsType<Contact>(result.Value);
        }

        [Fact]
        public async void TestUpdate()
        {
            var id = Guid.NewGuid().ToString();
            await insert(id);
            var result = await _controller.GetById(id);
            result.Value.Email = "rubenorlandochoque@gmail.com";
            await _controller.Update(result.Value);
            var resultTest = await _controller.GetById(id);
            Assert.Equal("rubenorlandochoque@gmail.com", resultTest.Value.Email);
        }

        [Fact]
        public async void TestSearch()
        {
            var id = Guid.NewGuid().ToString();
            await insert(id);
            var result = await _controller.Search("ruben@gmail.com");
            Assert.IsType<Contact>(result.Value);
        }

        [Fact]
        public async void TestGetByAddress()
        {
            var id = Guid.NewGuid().ToString();
            await insert(id);
            var contacts = await _controller.allByAddress("salta");
            Assert.True(contacts.Count > 0);
        }


        [Fact]
        public async void TestDelete()
        {
            var id = Guid.NewGuid().ToString();
            await insert(id);
            await _controller.DeleteById(id);
            var result = await _controller.GetById(id);
            Assert.True(result.Value == null);
        }
    }
}