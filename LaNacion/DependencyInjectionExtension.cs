﻿using LaNacion.Models;
using LaNacion.Models.Contacts;

namespace LaNacion
{
    public static class DependencyInjectionExtension
    {
        public static void InjectDependencies(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IContactRepository), typeof(ContactRepository));
        }
    }
}
