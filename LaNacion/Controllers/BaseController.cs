﻿using LaNacion.Models;
using LaNacion.Models.Contacts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LaNacion.Controllers
{
    public class BaseController<T> : ControllerBase where T : Base, new()
    {
        public readonly IRepository<T> Repository;

        protected BaseController(IRepository<T> repository)
        {
            Repository = repository;
        }

        [HttpPost, Route("add")]
        public async Task<ActionResult<int>> Insert([FromBody] T value)
        {
            return await Repository.Insert(value);
        }

        [HttpPut, Route("edit")]
        public async Task<ActionResult<int>> Update([FromBody] T value)
        {
            return await Repository.Update(value);
        }

        [HttpDelete, Route("delete/{id}")]
        public async Task<ActionResult> DeleteById(string id)
        {
            var item = await Repository.GetById(id);
            if (item == null)
            {
                return NotFound();
            }
            await Repository.Delete(id);
            return NoContent();
        }

        [HttpGet, Route("id/{id}")]
        public async Task<ActionResult<T>> GetById(string id)
        {
            var item = await Repository.GetById(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }
    }
}
