﻿using Microsoft.AspNetCore.Mvc;
using LaNacion.Models;
using LaNacion.Models.Contacts;

namespace LaNacion.Controllers
{
    [Route("api/[controller]")]
    public class ContactsController : BaseController<Contact>
    {
        private readonly IContactRepository _contactRepository;
        public ContactsController(IRepository<Contact> repository, IContactRepository contactRepository) : base(repository)
        {
            _contactRepository = contactRepository;
        }

        [HttpGet, Route("search/{value}")]
        public async Task<ActionResult<Contact>> Search(string value)
        {
           var item = await _contactRepository.GetByQuery(e => e.Email.Equals(value) || e.PhoneNumber.Equals(value));
            if(item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpGet, Route("allByAddress/{value}")]
        public async Task<ICollection<Contact>> allByAddress(string value)
        {
            return await _contactRepository.GetAll(e => string.Equals(e.Address, value, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
