﻿using System.ComponentModel.DataAnnotations;

namespace LaNacion.Models
{
    public class Base
    {
        [StringLength(64)]
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
