﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace LaNacion.Models
{
    public class Repository<T> : IRepository<T> where T : Base, new()
    {
        public readonly ApplicationDbContext Context;

        public Repository(ApplicationDbContext context)
        {
            Context = context;
        }

        public async Task<int> Insert(T entity)
        {
            await Context.Set<T>().AddAsync(entity);
            return await Context.SaveChangesAsync();
        }

        public async Task<T> GetById(string id)
        {
            return await Context.Set<T>().FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async Task<int> Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id)
        {
            var entity = await GetById(id);
            Context.Set<T>().Remove(entity);
            return await Context.SaveChangesAsync();
        }

        public async Task<T> GetByQuery(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<ICollection<T>> GetAll(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().Where(predicate).ToListAsync();
        }
    }
}
