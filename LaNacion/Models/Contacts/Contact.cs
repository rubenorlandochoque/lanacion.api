﻿using System.ComponentModel.DataAnnotations;

namespace LaNacion.Models.Contacts
{
    public class Contact: Base
    {
        [StringLength(64)]
        public string Name { get; set; } 
        [StringLength(64)]
        public string Company { get; set; }
        [StringLength(64)]
        public string ProfileImage { get; set; }
        [StringLength(64)]
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
        [StringLength(64)]
        public string PhoneNumber { get; set; }
        [StringLength(512)]
        public string Address { get; set; }
    }
}
