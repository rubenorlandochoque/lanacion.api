﻿using LaNacion.Models.Contacts;
using Microsoft.EntityFrameworkCore;

namespace LaNacion.Models
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }
    }
}
