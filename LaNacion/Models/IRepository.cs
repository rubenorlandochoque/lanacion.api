﻿using System.Linq.Expressions;

namespace LaNacion.Models
{
    public interface IRepository<T> where T : Base, new()
    {
        Task<int> Insert(T entity);
        Task<T> GetById(string id);

        Task<int> Update(T entity);

        Task<int> Delete(string id);

        Task<T> GetByQuery(Expression<Func<T, bool>> predicate);
        Task<ICollection<T>> GetAll(Expression<Func<T, bool>> predicate);
    }
}
